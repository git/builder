#!/bin/sh

# The script is entrypoint for container workers.
# The following environment variables must by set
# by the corresponding Container files:
#
# - IMAGE_NAME - name of the image
# - CCACHE_LIBDIR - path where to search for ccache (usually /usr/lib64/ccache)

# Use the WORKERNAME to create different worker dirs in case
# we have multiple containers running on the same build host (using the same
# shared/ directory). Note that a latent worker only runs one container at
# a time. Even if it might do multiple builds it will do those builds using
# the same container/image. The latent worker only has one instance of
# the worker/container running at the same time. But multiple different
# latent workers might be running on the same host. Those latent workers
# all have a different WORKERNAME.
worker_dir=shared/$WORKERNAME/worker
tac_file=$worker_dir/buildbot.tac

# Always create a new tac file to force buildbot-worker update
rm -f $tac_file
mkdir -p $worker_dir
buildbot-worker create-worker --force $worker_dir \
	$BUILDMASTER:$BUILDMASTER_PORT $WORKERNAME $WORKERPASS

# Fill in the info visible in the buildbot website
# objcopy gives us the binutils version, iconv the glibc version
echo buildbot@sourceware.org > $worker_dir/info/admin
echo $IMAGE_NAME > $worker_dir/info/host
gcc --version | head -1 >> $worker_dir/info/host
objcopy --version | head -1 >> $worker_dir/info/host
iconv --version | head -1 >> $worker_dir/info/host

# We don't want to leak this in any logs
unset WORKERPASS

# Make sure ccache is in the PATH and uses a shared cache
# There is one ccache dir per image shared between workers
export PATH=$CCACHE_LIBDIR:$PATH
mkdir -p shared/$IMAGE_NAME/ccache
export CCACHE_DIR=/home/builder/shared/$IMAGE_NAME/ccache

# We like a high, but not too high open file limit
# In some cases docker seems to set it to something really
# high, like 1048576, which causes issues for valgrind
# https://bugs.kde.org/show_bug.cgi?id=465435
ulimit -n 20480 || true

# Use --nodaemon because we don't want to exit the container
buildbot-worker start --nodaemon $worker_dir
