#!/bin/bash

set -e

HERE=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)

SNAPSHOTSDIR=/var/snapshots

PUBLISHDIR=$HOME/shared
PUBLISHOUTPUT=output
PUBLISHFILE=publish

# unique date string
PUBLISHDATE=$(date -u +%Y-%m-%d_%H-%M_%s)

# where we keep the files while publishing
PUBLISHINGDIR=$HOME/publishing

# Make sure only one process is publishing
LOCKFILE=$HOME/publish_lock
exec 200>$LOCKFILE
flock -n 200 || exit 1

if [ -f $PUBLISHDIR/$PUBLISHFILE ]; then
  # Make sure publishing dir is clean
  rm -rf $PUBLISHINGDIR
  mkdir -p $PUBLISHINGDIR

  # Move output and publish file for processing
  mv $PUBLISHDIR/$PUBLISHOUTPUT $PUBLISHDIR/$PUBLISHFILE $PUBLISHINGDIR

  # Create sha512.sum files
  $HERE/make-sha $PUBLISHINGDIR/$PUBLISHOUTPUT

  # Where to publish, sanity check given subdir
  read PUBLISHSUBDIR < $PUBLISHINGDIR/$PUBLISHFILE
  if echo $PUBLISHSUBDIR | grep -E '^[0-9a-zA-Z][0-9a-zA-Z/_-]{1,64}$'; then
    mkdir -p $SNAPSHOTSDIR/$PUBLISHSUBDIR

    # We need a copy to get the selinux context correct
    cp -r $PUBLISHINGDIR/$PUBLISHOUTPUT $SNAPSHOTSDIR/$PUBLISHSUBDIR/$PUBLISHDATE
    # Update latest link
    cd $SNAPSHOTSDIR/$PUBLISHSUBDIR
    rm -f latest
    ln -s $PUBLISHDATE latest

    # touch all parent dirs to update last modified time
    pdirs=$PUBLISHSUBDIR
    cd $SNAPSHOTSDIR
    while [ $pdirs != "." ]; do touch $pdirs; pdirs=$(dirname $pdirs); done
  else
    echo BAD subdir
  fi

  rm -rf $PUBLISHINGDIR
fi
