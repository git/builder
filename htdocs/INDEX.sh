#! /bin/sh

# assemble index.html from parts

set -e
exec > index.html.new
cat index.html.pre

echo '<table>'
lastproject=
curl -s https://builder.sourceware.org/buildbot/api/v2/builders \
  | jq -r '[ .builders[] | { "name": .name, "ids": .masterids | length } ]
           | map(select(.ids > 0)) | [.[].name] | sort | .[]' |
    egrep -v '__Janitor|sourceware' |
    while read projbuilder
do
    if expr "$projbuilder" : "binutils-gdb" >/dev/null; then
        project="`echo $projbuilder | cut -f1,2 -d-`"
        builder="`echo $projbuilder | cut -f3- -d-`"
    else
        project="`echo $projbuilder | cut -f1 -d-`"
        builder="`echo $projbuilder | cut -f2- -d-`"
    fi
    projectlink='<td><a href="/buildbot/#/builders?tags='$project'">'$project'</a> <a rel="nofollow" href="/testruns/?has_keyvalue_op=glob&has_keyvalue_k=testrun.gitdescribe&has_keyvalue_v=buildbot/'$project-'%2A"><img width="16" height="16" src="/testruns/favicon.ico"/></a></td>'    
    if [ "$lastproject" = "" ]; then
        echo '<tr>'"$projectlink"'<td width="100%">'
        bar=''
    elif [ "$lastproject" != "$project" ]; then
        echo '</td></tr><tr>'"$projectlink"'<td width="100%">'
        bar=''
    fi
    echo -n "$bar"'<span><a href="/buildbot/#builders/'$projbuilder'"><img src="/buildbot/badges/'$projbuilder'.svg?left_text=%20%20'$builder'%20%20"></a>'
    echo '<a rel="nofollow" href="/testruns/?has_keyvalue_op=glob&has_keyvalue_k=testrun.gitdescribe&has_keyvalue_v=buildbot/'$projbuilder'/%2A"><img width="16" height="16" src="/testruns/favicon.ico"/></a></span> '
    # ... how in html/css tarnation can one glue the preceding two elements together
    # to avoid line-wrap splitting them up?
    lastproject=$project
    bar=' '
done
echo '</td></tr></table>'
echo '<p class="tight">inventory as of '`date +%Y-%m-%d`'</p>'

cat index.html.post
mv index.html.new index.html
